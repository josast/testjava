package org.josast.station;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;

public class GroundStationJsonManager {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static void saveToJson(List<GroundStation> stations, String filePath) throws IOException {
        objectMapper.writeValue(new File(filePath), stations);
    }

    public static List<GroundStation> loadFromJson(String filePath) throws IOException {
        CollectionType listType = objectMapper.getTypeFactory().constructCollectionType(ArrayList.class, GroundStation.class);
        return objectMapper.readValue(new File(filePath), listType);
    }

    public static Optional<GroundStation> getDefaultStation(List<GroundStation> stations) {
        return stations.stream().filter(GroundStation::isDefault).findFirst();
    }

    public static void setDefaultStation(List<GroundStation> stations, GroundStation defaultStation) {
        stations.forEach(station -> station.setDefault(station == defaultStation));
    }

    // Exemple d'utilisation
    public static void main(String[] args) {
        try {
            List<GroundStation> stations = new ArrayList<>();
            stations.add(new GroundStation("Station 1", 100.0, 45.0, 30.0));
            stations.add(new GroundStation("Station 2", 200.0, 50.0, 35.0));

            saveToJson(stations, "ground_stations.json");
            System.out.println("Stations sauvegardées avec succès.");

            List<GroundStation> loadedStations = loadFromJson("ground_stations.json");
            System.out.println("Stations chargées :");
            loadedStations.forEach(System.out::println);

            Optional<GroundStation> defaultStation = getDefaultStation(loadedStations);
            defaultStation.ifPresent(station -> 
                System.out.println("Station par défaut : " + station.getName()));

            // Changer la station par défaut
            if (loadedStations.size() > 1) {
                setDefaultStation(loadedStations, loadedStations.get(1));
                System.out.println("Nouvelle station par défaut définie.");
                saveToJson(loadedStations, "ground_stations.json");
                System.out.println("Stations mises à jour sauvegardées.");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
