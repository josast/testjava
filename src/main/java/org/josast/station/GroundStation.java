/** */
package org.josast.station;

/**
 * Cette classe représente une station au sol pour un radioamateur. Elle contient les informations
 * suivantes : nom, altitude, longitude et latitude.
 */
public class GroundStation implements GroundStationInterface {
  private String nom;
  private double altitude;
  private double longitude;
  private double latitude;
  private boolean isDefault=false;

  /**
   * Constructeur de la classe GroundStation.
   *
   * @param nom Le nom de la station.
   * @param altitude L'altitude de la station.
   * @param longitude La longitude de la station.
   * @param latitude La latitude de la station.
   */
  public GroundStation(String nom, double altitude, double longitude, double latitude) {
    this.nom = nom;
    this.altitude = altitude;
    this.longitude = longitude;
    this.latitude = latitude;
    this.isDefault=false;
  }

  /**
   * Retourne le nom de la station.
   *
   * @return Le nom de la station.
   */
  
  public String getName() {
    return nom;
  }

  /**
   * Modifie le nom de la station.
   *
   * @param nom Le nouveau nom de la station.
   */
 
  public void setName(String nom) {
    this.nom = nom;
  }

  /**
   * Retourne l'altitude de la station.
   *
   * @return L'altitude de la station.
   */

  public double getAltitude() {
    return altitude;
  }

  /**
   * Modifie l'altitude de la station.
   *
   * @param altitude La nouvelle altitude de la station.
   */

  public void setAltitude(double altitude) {
    this.altitude = altitude;
  }

  /**
   * Retourne la longitude de la station.
   *
   * @return La longitude de la station.
   */

  public double getLongitude() {
    return longitude;
  }

  /**
   * Modifie la longitude de la station.
   *
   * @param longitude La nouvelle longitude de la station.
   */

  public void setLongitude(double longitude) {
    this.longitude = longitude;
  }

  /**
   * Retourne la latitude de la station.
   *
   * @return La latitude de la station.
   */
 
  public double getLatitude() {
    return latitude;
  }

  /**
   * Modifie la latitude de la station.
   *
   * @param latitude La nouvelle latitude de la station.
   */

  public void setLatitude(double latitude) {
    this.latitude = latitude;
  }

  // Getters et setters existants...

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }



  /**
   * Retourne une chaîne de caractères représentant la station.
   *
   * @return Une chaîne de caractères représentant la station.
   */
  @Override
   // Mise à jour de toString()
    public String toString() {
        return "GroundStation{" +
                "nom='" + nom + '\'' +
                ", altitude=" + altitude +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", isDefault=" + isDefault +
                '}';
    }
  }


