package org.josast.station;

public interface GroundStationInterface {

  /**
   * Retourne le nom de la station.
   *
   * @return Le nom de la station.
   */
  String getName();

  /**
   * Modifie le nom de la station.
   *
   * @param nom Le nouveau nom de la station.
   */
  void setName(String nom);

  /**
   * Retourne l'altitude de la station.
   *
   * @return L'altitude de la station.
   */
  double getAltitude();

  /**
   * Modifie l'altitude de la station.
   *
   * @param altitude La nouvelle altitude de la station.
   */
  void setAltitude(double altitude);

  /**
   * Retourne la longitude de la station.
   *
   * @return La longitude de la station.
   */
  double getLongitude();

  /**
   * Modifie la longitude de la station.
   *
   * @param longitude La nouvelle longitude de la station.
   */
  void setLongitude(double longitude);

  /**
   * Retourne la latitude de la station.
   *
   * @return La latitude de la station.
   */
  double getLatitude();

  /**
   * Modifie la latitude de la station.
   *
   * @param latitude La nouvelle latitude de la station.
   */
  void setLatitude(double latitude);


    boolean isDefault();

    void setDefault(boolean isDefault);


  /**
   * Retourne une chaîne de caractères représentant la station.
   *
   * @return Une chaîne de caractères représentant la station.
   */
  String toString();
}

