package org.josast.satdata;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.net.URL;


public class SatYamlProcessor {


    public void process(String urlin)
    {
       try {
       
       URL url = new URL(urlin);
        Yaml yaml = new Yaml(new Constructor(SatelliteConfig.class));

       
            InputStream inputStream = url.openStream();
 //       InputStream inputStream = SatelliteConfig.class
 //           .getClassLoader()
 //           .getResourceAsStream("satellite.yaml");
        SatelliteConfig config = yaml.load(inputStream);



        System.out.println("Satellite Name: " + config.getName());
        System.out.println("NORAD ID: " + config.getNorad());

        System.out.println("\nData:");
        for (Map.Entry<String, Object> entry : config.getData().entrySet()) {
            System.out.println("  " + entry.getKey() + ": " + entry.getValue());
        }

        System.out.println("\nTransmitters:");
        for (Map.Entry<String, Transmitter> entry : config.getTransmitters().entrySet()) {
            Transmitter transmitter = entry.getValue();
            System.out.println("  " + entry.getKey() + ":");
            System.out.println("    Frequency: " + transmitter.getFrequency());
            System.out.println("    Modulation: " + transmitter.getModulation());
            System.out.println("    Baudrate: " + transmitter.getBaudrate());
            System.out.println("    Framing: " + transmitter.getFraming());
            System.out.println("    Data: " + transmitter.getData());
        }
    }         catch (Exception e) {
            e.printStackTrace();
        }


    }

}
