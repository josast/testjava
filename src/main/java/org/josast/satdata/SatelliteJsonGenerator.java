package org.josast.satdata;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.io.*;
import java.net.*;
import java.util.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class SatelliteJsonGenerator {



    public  List<Satellite>  generateJson ( String xmlUrl) {

         List<Satellite> satellites = new ArrayList<>();

        try {
            // URL du fichier XML
          // String xmlUrl = "https://code.electrolab.fr/xtof/AMSATLIST/-/raw/master/Data/AmsatList.xml";

            // Télécharger et parser le XML
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new URL(xmlUrl).openStream());

            // Normaliser le document XML
            doc.getDocumentElement().normalize();

            // Créer une liste pour stocker les satellites
           
            // Obtenir la liste des éléments "sat"
            NodeList satList = doc.getElementsByTagName("Satellites");
            System.out.println("nb sat Sml"+ satList.getLength());

            // Parcourir chaque élément "sat"
            for (int i = 0; i < satList.getLength(); i++) {
                Node satNode = satList.item(i);
                if (satNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element satElement = (Element) satNode;
                    String name = getElementTextContent(satElement, "Name");
                    Integer noradId = getElementIntContent(satElement, "NasaID");
                    String status = getElementTextContent(satElement, "Status");
                    String internationalId = getElementTextContent(satElement, "InternationalID");

                    // Créer un objet Satellite et l'ajouter à la liste
                    satellites.add(new Satellite(name, noradId, status, internationalId));
                }
            }

            // Utiliser Gson pour convertir la liste en JSON
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            String json = gson.toJson(satellites);

         //    System.out.println (json);

            // Écrire le JSON dans un fichier
           // É  try (FileWriter file = new FileWriter("satellites.json")) {
             // É // É    file.write(json);
           //     System.out.println("Fichier JSON généré avec succès.");
          // É   }

           } catch (Exception e) {
            e.printStackTrace();
         }
         return satellites;
    }

       // Méthode utilitaire pour obtenir le contenu textuel d'un élément
    private static String getElementTextContent(Element parent, String elementName) {
        NodeList nodeList = parent.getElementsByTagName(elementName);
        if (nodeList != null && nodeList.getLength() > 0) {
            Node node = nodeList.item(0);
            return node.getTextContent().trim();
        }
        return "";
    }

       // Méthode utilitaire pour obtenir le contenu entier d'un élément
    private static Integer getElementIntContent(Element parent, String elementName) {
        String content = getElementTextContent(parent, elementName);
        try {
            return Integer.parseInt(content);
        } catch (NumberFormatException e) {
            System.err.println("Erreur de conversion pour NORAD ID: " + content);
            return null;
        }
    }
}