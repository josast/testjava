package org.josast.satdata;

import java.util.*;

public class AmateurFrequencyCheck {

private   static final double[][] FREQUENCY_RANGES = {
        {7.000, 7.100},
        {14.000, 14.250},
        {18.068, 18.168},
        {21.000, 21.450},
        {24.890, 24.990},
        {28.000, 29.700},
        {144.0, 146.0},
        {435.0, 438.0},
        {1260.0, 1270.0},
        {2400.0, 2450.0},
        {3400.0, 3410.0},
        {5650.0, 5670.0},
        {5830.0, 5850.0},
        {10450.0, 10500.0},
        {24000.0, 24050.0},
        {47000.0, 47200.0},
        {76000.0, 81000.0},
        {134000.0, 136000.0},
        {136000.0, 141000.0},
        {241000.0, 248000.0},
        {248000.0, 250000.0}
    };
 

 public static boolean isAmateurSatelliteFrequency(double frequency) {

    boolean isAmateur = false;

        for (double[] range : FREQUENCY_RANGES) {
            if (!isAmateur)
            {
            if (frequency >= range[0] && frequency <= range[1]) {
                 isAmateur = true;
            }
             }
        }
        return isAmateur;
    }



    
}