package org.josast.satdata;

     public  class Satellite {
        String name;
        Integer norad_id;
        String status;
        String internationalId;

      public  Satellite(String name, Integer norad_id) {
            this.name = name;
            this.norad_id = norad_id;
        }

    Satellite(String name, Integer norad_id, String status, String internationalId) {
            this.name = name;
            this.norad_id = norad_id;
            this.status = status;
            this.internationalId = internationalId;
        }

    public Integer getNoradId()
    {
        return this.norad_id;
    }

    }