package org.josast.satdata;


import com.google.gson.annotations.SerializedName;
import java.util.List;

public class AmsatSatellite {
    @SerializedName("name")
    private String name;

    @SerializedName("status")
    private String status;

    @SerializedName("uplink")
    private String uplink;

    @SerializedName("downlink")
    private String downlink;

    @SerializedName("beacon")
    private String beacon;

    @SerializedName("mode")
    private String mode;

    @SerializedName("callsign")
    private String callsign;

    @SerializedName("norad_id")
    private int noradid;


    // Constructeur
    public AmsatSatellite() {}

    // Getters et Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUplink() {
        return uplink;
    }

    public void setUplink(String uplink) {
        this.uplink = uplink;
    }

    public String getDownlink() {
        return downlink;
    }

    public void setDownlink(String downlink) {
        this.downlink = downlink;
    }

    public String getBeacon() {
        return beacon;
    }

    public void setBeacon(String beacon) {
        this.beacon = beacon;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCallsign() {
        return callsign;
    }

    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }
    public int getNoradId() {
        return noradid;
    }

    public void setNoradId(int callsign) {
        this.noradid = noradid;
    }



    @Override
    public String toString() {
        return "AmsatSatellite{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", uplink=" + uplink +
                ", downlink=" + downlink +
                ", beacon=" + beacon +
                ", mode=" + mode +
                ", callsign='" + callsign + '\'' +
                '}';
    }
}