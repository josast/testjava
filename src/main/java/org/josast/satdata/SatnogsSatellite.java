package org.josast.satdata;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class SatnogsSatellite {
    @SerializedName("name")
    private String name;

    @SerializedName("norad_cat_id")
    private int noradCatId;

    @SerializedName("names")
    private String names;

    @SerializedName("image")
    private String image;

    @SerializedName("status")
    private String status;

    @SerializedName("decayed")
    private boolean decayed;

    @SerializedName("launched")
    private String launched;

    @SerializedName("deployed")
    private String deployed;

    @SerializedName("website")
    private String website;

    @SerializedName("operator")
    private String operator;

    @SerializedName("countries")
    private String countries;

    @SerializedName("telemetries")
    private List<Telemetry> telemetries;



    // Constructeur
    public SatnogsSatellite() {}

    // Getters et Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoradCatId() {
        return noradCatId;
    }

    public void setNoradCatId(int noradCatId) {
        this.noradCatId = noradCatId;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isDecayed() {
        return decayed;
    }

    public void setDecayed(boolean decayed) {
        this.decayed = decayed;
    }

    public String getLaunched() {
        return launched;
    }

    public void setLaunched(String launched) {
        this.launched = launched;
    }

    public String getDeployed() {
        return deployed;
    }

    public void setDeployed(String deployed) {
        this.deployed = deployed;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getCountries() {
        return countries;
    }

    public void setCountries(String countries) {
        this.countries = countries;
    }

    public List<Telemetry> getTelemetries() {
        return telemetries;
    }

    public void setTelemetries(List<Telemetry> telemetries) {
        this.telemetries = telemetries;
    }


    // Classes internes pour représenter les structures imbriquées
    public static class Telemetry {
        @SerializedName("schema")
        private String schema;

        // Getters et Setters pour Telemetry
    }

    public static class Transmitter {
        @SerializedName("description")
        private String description;

        @SerializedName("alive")
        private boolean alive;

        @SerializedName("type")
        private String type;

        @SerializedName("uplink_low")
        private Long uplinkLow;

        @SerializedName("uplink_high")
        private Long uplinkHigh;

        @SerializedName("uplink_drift")
        private Integer uplinkDrift;

        @SerializedName("downlink_low")
        private Long downlinkLow;

        @SerializedName("downlink_high")
        private Long downlinkHigh;

        @SerializedName("downlink_drift")
        private Integer downlinkDrift;

        @SerializedName("mode")
        private String mode;

        @SerializedName("invert")
        private boolean invert;

        @SerializedName("baud")
        private Integer baud;

        @SerializedName("norad_follow")
        private Integer noradFollow;

        // Getters et Setters pour Transmitter
    }

    @Override
    public String toString() {
        return "SatnogsSatellite{" +
                "name='" + name + '\'' +
                ", noradCatId=" + noradCatId +
                ", names=" + names +
                ", status='" + status + '\'' +
                ", decayed=" + decayed +
                // ... autres champs ...
                '}';
    }
}