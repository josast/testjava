
package org.josast.satdata;

import java.util.ArrayList;
import java.util.List;

public class CombinedSatellite {
    private String name;
    private int norad;
    private AmsatSatellite amsatData = null;
    private SatnogsSatellite satnogsData;
    private List<Double> frequencies = null;
    private boolean isAmateur = false;


    public CombinedSatellite(int norad, AmsatSatellite amsatData, SatnogsSatellite satnogsData) {
        this.norad = norad;
      if (amsatData != null )
      {
          this.name = amsatData.getName();
         frequencies = extractFrequencies(amsatData.getDownlink());
         if(frequencies.size() > 0)
         {
             for (int i=0; i<frequencies.size(); i++)
            {
                if (!isAmateur) 
                {
                   isAmateur = AmateurFrequencyCheck.isAmateurSatelliteFrequency( frequencies.get(i));
                }
            }

         }
      }
        else if (satnogsData != null )
        this.name = satnogsData.getName();
        this.amsatData = amsatData;
        this.satnogsData = satnogsData;
       

    }

    public AmsatSatellite getAmsatData()
    {
        return this.amsatData;
    }

    public Integer getNorad()
    {
        return this.norad;
    }

    public String getName()
    {
        return this.name;
    }


        private  List<Double> extractFrequencies(String input) {
        List<Double> frequencies = new ArrayList<>();

        if (input!= null)
        {

   
        String[] parts = input.split("\\\\/");

        for (String part : parts) {
            try {
                if (part.contains("-")) {
                    String[] range = part.split("-");
                    double start = Double.parseDouble(range[0]);
                    frequencies.add(start);
                    double end = Double.parseDouble(range[1]);
                    frequencies.add(end);
                  //  frequencies.add(new double[]{start, end});
                } else {
                    double frequency = Double.parseDouble(part);
                    frequencies.add(frequency);
                }
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
               System.out.println(" erreur ");
            }
        }
        
 //       for (String part : parts) {
  //             try {
  //                 double frequency = Double.parseDouble(part);
  //                 frequencies.add(frequency);
  //             } catch (NumberFormatException e) {
  //                 // Ignorer les parties qui ne peuvent pas être converties en double
  //             }
   //        }
        }
        
        return frequencies;
    }

    // Getters et setters
    // ...

    @Override
    public String toString() {
        return "CombinedSatellite{" +
                "name='" + name + '\'' +
                "norad='" + norad + '\'' +
                "isAmateur='" + isAmateur + '\'' +
                ", amsatData=" + (amsatData != null ? amsatData.getStatus() : "null") +
                ", satnogsData=" + (satnogsData != null ? satnogsData.getStatus() : "null") +
                '}';
    }
}