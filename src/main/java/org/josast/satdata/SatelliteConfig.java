package org.josast.satdata;


 

 
import java.util.List;
import java.util.Map;

public class SatelliteConfig {
    private String name;
    private List<String> alternativeNames;
    private int norad;
    private Map<String, Object> data;
    private Map<String, Transmitter> transmitters;

    // Getters and setters

    


    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getAlternativeNames() {
        return alternativeNames;
    }

    public void setAlternativeNames(List<String> alternativeNames) {
        this.alternativeNames = alternativeNames;
    }

    public int getNorad() {
        return norad;
    }

    public void setNorad(int norad) {
        this.norad = norad;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public Map<String, Transmitter> getTransmitters() {
        return transmitters;
    }

    public void setTransmitters(Map<String, Transmitter> transmitters) {
        this.transmitters = transmitters;
    }
}
