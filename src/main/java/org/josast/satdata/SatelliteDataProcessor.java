package org.josast.satdata;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class SatelliteDataProcessor {

/*
    public static void main(String[] args) {
        Gson gson = new Gson();

        try {
            // Lecture du premier fichier JSON (AMSAT)
            String amsatJson = readJsonFromUrl("https://raw.githubusercontent.com/palewire/ham-satellite-database/main/data/amsat-all-frequencies.json");
            List<AmsatSatellite> amsatSatellites = gson.fromJson(amsatJson, new TypeToken<List<AmsatSatellite>>(){}.getType());

            // Lecture du deuxième fichier JSON (SatNOGS)
            String satnogsJson = readJsonFromUrl("https://raw.githubusercontent.com/palewire/ham-satellite-database/main/data/satnogs.json");
            List<SatnogsSatellite> satnogsSatellites = gson.fromJson(satnogsJson, new TypeToken<List<SatnogsSatellite>>(){}.getType());

            // Traitement des données concaténées
            processSatelliteData(amsatSatellites, satnogsSatellites);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
*/


    public  String readJsonFromUrl(String urlString) throws Exception {
        StringBuilder json = new StringBuilder();
        URL url = new URL(urlString);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
        }
        return json.toString();
    }

    public  void processSatelliteData(List<AmsatSatellite> amsatSatellites, List<SatnogsSatellite> satnogsSatellites) {
        // Logique de traitement des données concaténées
        System.out.println("Nombre de satellites AMSAT : " + amsatSatellites.size());
        System.out.println("Nombre de satellites SatNOGS : " + satnogsSatellites.size());

        // Ajoutez ici votre logique de traitement
    }


     public List<CombinedSatellite> combineSatelliteData(List<AmsatSatellite> amsatSatellites, List<SatnogsSatellite> satnogsSatellites) {
        // Créer des maps pour un accès rapide par nom
        Map<Integer, AmsatSatellite> amsatMap = amsatSatellites.stream()
                .collect(Collectors.toMap(AmsatSatellite::getNoradId, s -> s, (s1, s2) -> s1));

        Map<Integer, SatnogsSatellite> satnogsMap = satnogsSatellites.stream()
                .collect(Collectors.toMap(SatnogsSatellite::getNoradCatId, s -> s, (s1, s2) -> s1));

        // Combiner tous les noms uniques
        Set<Integer> allNorad = new HashSet<>();
        allNorad.addAll(amsatMap.keySet());
        allNorad.addAll(satnogsMap.keySet());

        // Créer la liste combinée
        List<CombinedSatellite> combinedList = new ArrayList<>();
        for (Integer noradid : allNorad) {
            AmsatSatellite amsatData = amsatMap.get(noradid);
            SatnogsSatellite satnogsData = satnogsMap.get(noradid);

            combinedList.add(new CombinedSatellite(noradid, amsatData, satnogsData));
        }

        return combinedList;
    }

    public List<CombinedSatellite> getCombinedSatellitesWithAmsatData( List<CombinedSatellite> allCombined ) {
    
    
    return allCombined.stream()
        .filter(satellite -> satellite.getAmsatData() != null)
        .collect(Collectors.toList());
}

}
