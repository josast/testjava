package com.mycompany.app;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Satellite {
    private String name;
    private String noradId;
    private String uplink;
    private String downlink;
    private String beacon;
    private String mode;
    private String callsign;
    private String status;
    private String satnogsId;

    // Getters and Setters

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("norad_id")
    public String getNoradId() {
        return noradId;
    }

    @JsonProperty("norad_id")
    public void setNoradId(String noradId) {
        this.noradId = noradId;
    }

    @JsonProperty("uplink")
    public String getUplink() {
        return uplink;
    }

    @JsonProperty("uplink")
    public void setUplink(String uplink) {
        this.uplink = uplink;
    }

    @JsonProperty("downlink")
    public String getDownlink() {
        return downlink;
    }

    @JsonProperty("downlink")
    public void setDownlink(String downlink) {
        this.downlink = downlink;
    }

    @JsonProperty("beacon")
    public String getBeacon() {
        return beacon;
    }

    @JsonProperty("beacon")
    public void setBeacon(String beacon) {
        this.beacon = beacon;
    }

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    @JsonProperty("callsign")
    public String getCallsign() {
        return callsign;
    }

    @JsonProperty("callsign")
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("satnogs_id")
    public String getSatnogsId() {
        return satnogsId;
    }

    @JsonProperty("satnogs_id")
    public void setSatnogsId(String satnogsId) {
        this.satnogsId = satnogsId;
    }
}
