package com.mycompany.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public class SatelliteDeserializer {
    public static void main(String[] args) {
        String json = "[\n" +
                "  {\n" +
                "    \"name\":\"1KUNS-3\",\n" +
                "    \"norad_id\":\"47941\",\n" +
                "    \"uplink\":null,\n" +
                "    \"downlink\":\"435.310\",\n" +
                "    \"beacon\":null,\n" +
                "    \"mode\":\"1k2\\/9k6* FSK\",\n" +
                "    \"callsign\":null,\n" +
                "    \"status\":\"active\",\n" +
                "    \"satnogs_id\":\"JHGM-7853-8293-9677-2181\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\":\"1KUNS-PF\",\n" +
                "    \"norad_id\":\"43466\",\n" +
                "    \"uplink\":null,\n" +
                "    \"downlink\":\"437.300\",\n" +
                "    \"beacon\":null,\n" +
                "    \"mode\":\"1k2-9k6* GMSK\",\n" +
                "    \"callsign\":null,\n" +
                "    \"status\":\"re-entered\",\n" +
                "    \"satnogs_id\":\"VWFS-8293-1536-2839-9284\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\":\"3B5GSAT\",\n" +
                "    \"norad_id\":\"47961\",\n" +
                "    \"uplink\":null,\n" +
                "    \"downlink\":\"437.130\",\n" +
                "    \"beacon\":null,\n" +
                "    \"mode\":null,\n" +
                "    \"callsign\":null,\n" +
                "    \"status\":\"unknown\",\n" +
                "    \"satnogs_id\":\"DZKC-2045-5925-0631-5801\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"name\":\"3CAT-1\",\n" +
                "    \"norad_id\":\"43728\",\n" +
                "    \"uplink\":null,\n" +
                "    \"downlink\":\"437.250\",\n" +
                "    \"beacon\":\"437.250\",\n" +
                "    \"mode\":\"9600bps BPSK CW\",\n" +
                "    \"callsign\":null,\n" +
                "    \"status\":\"re-entered\",\n" +
                "    \"satnogs_id\":\"HIHZ-2146-5294-1802-1615\"\n" +
                "  }\n" +
                "]";

        ObjectMapper objectMapper = new ObjectMapper();
        try {
            List<Satellite> satellites = objectMapper.readValue(json, new TypeReference<List<Satellite>>() {});
            for (Satellite satellite : satellites) {
                System.out.println(satellite.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
