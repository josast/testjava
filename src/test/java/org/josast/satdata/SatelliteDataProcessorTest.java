package org.josast.satdata;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Collectors;
import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;


public class SatelliteDataProcessorTest 
{



@Test
    public void testApp() {

        SatelliteDataProcessor sdp = new SatelliteDataProcessor();
         Gson gson = new Gson();

        try {
            // Lecture du premier fichier JSON (AMSAT)
            String amsatJson = sdp.readJsonFromUrl("https://raw.githubusercontent.com/palewire/ham-satellite-database/main/data/amsat-all-frequencies.json");
            List<AmsatSatellite> amsatSatellites = gson.fromJson(amsatJson, new TypeToken<List<AmsatSatellite>>(){}.getType());

            // Lecture du deuxième fichier JSON (SatNOGS)
            String satnogsJson = sdp.readJsonFromUrl("https://raw.githubusercontent.com/palewire/ham-satellite-database/main/data/satnogs.json");
            List<SatnogsSatellite> satnogsSatellites = gson.fromJson(satnogsJson, new TypeToken<List<SatnogsSatellite>>(){}.getType());

            // Traitement des données concaténées
            sdp.processSatelliteData(amsatSatellites, satnogsSatellites);


             // Combinaison des données
            List<CombinedSatellite> combinedSatellites = sdp.combineSatelliteData(amsatSatellites, satnogsSatellites);

            // Affichage des résultats
            System.out.println("Nombre total de satellites combinés : " + combinedSatellites.size());
          //  combinedSatellites.forEach(System.out::println);

            // identifie les sat radioamateurs

            List<CombinedSatellite> combinedSatellitesAmsat =sdp.getCombinedSatellitesWithAmsatData(combinedSatellites);
             System.out.println("Nombre total de satellites combinés : " + combinedSatellites.size());
            combinedSatellitesAmsat.forEach(System.out::println);;

//            Map<Integer, Satellite> satelliteMap = combinedSatellitesAmsat.stream()
//    .collect(Collectors.toMap(combinedSatellitesAmsat::getNorad, Function.identity()));


            String xmlUrl = "https://code.electrolab.fr/xtof/AMSATLIST/-/raw/master/Data/AmsatList.xml";
            SatelliteJsonGenerator sjg = new SatelliteJsonGenerator();
            
           List<Satellite> xmlSat = sjg.generateJson(xmlUrl);
             System.out.println("Nombre total de satellites XML : " + xmlSat);

             Map<Integer, Satellite> satelliteMap = xmlSat.stream()
                  .collect(Collectors.toMap(Satellite::getNoradId, s -> s, (s1, s2) -> s1));

            for (int i=0; i<combinedSatellitesAmsat.size(); i++)
            {
               Integer noradId = combinedSatellitesAmsat.get(i).getNorad();
               if( satelliteMap.containsKey(noradId))
               {
                       System.out.println("Found :" +  noradId + " "+combinedSatellitesAmsat.get(i).getName());
               }else
               {
                    System.out.println(" Not Found :" +  noradId + " "+combinedSatellitesAmsat.get(i).getName());
               }
            }

            SatYamlProcessor syp = new SatYamlProcessor();
            syp.process("https://raw.githubusercontent.com/daniestevez/gr-satellites/main/python/satyaml/INSPIRE-SAT_7.yml");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }






}